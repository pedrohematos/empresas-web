import { createMuiTheme } from '@material-ui/core/styles'

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ee4c77',
      dark: '#ff0f44'

    },
    secondary: {
      light: '#0066ff',
      main: '#fff'
    }
  }

})
