export type Enterprise = {
  city: string
  country: string
  description: string
  email_enterprise: string
  enterprise_name: string
  enterprise_type: EnterpriseType
  facebook: string
  id: number
  linkedin: string
  own_enterprise: boolean
  phone: string
  photo: string
  share_price: number
  twitter: number
  value: number
}

export type EnterpriseType = {
  enterprise_type_name: string
  id: number
}
