import React, { createContext, useEffect, useState } from 'react'

import { getEnterprises } from '../../services/enterprises/index'
import { Enterprise } from '../../type/home/enterprise'

type HomeContextData = {
  showEnterprise: boolean
  showInputSearch: boolean
  setShowEntrise: (showEntrise: boolean) => void
  setFilter: (filter: string) => void
  setShowInputSearch: (showInputSearch: boolean) => void
  setEnterpriseCard: (enterpriseCard: EnterpriseCard) => void
  enterpriseCard: EnterpriseCard
  load: () => void
  enterprises: Enterprise[]
  setEnterprises: (enterprises: Enterprise[]) => void
  findedEnterprises: Enterprise[]
}

type EnterpriseCard = {
  description: string
  image: string
  name: string
}

const HomeContext = createContext<HomeContextData>({} as HomeContextData)

export const HomeProvider: React.FC = ({ children }) => {
  const [showInputSearch, setShowInputSearch] = useState<boolean>(false)
  const [showEnterprise, setShowEntrise] = useState<boolean>(false)
  const [filter, setFilter] = useState<string>('')
  const [enterprises, setEnterprises] = useState<Enterprise[]>([])
  const [findedEnterprises, setFindedEnterprises] = useState<Enterprise[]>([])
  const [enterpriseCard, setEnterpriseCard] = useState<EnterpriseCard>({
    description: '',
    image: '',
    name: ''
  })

  useEffect(() => {
    if (enterprises) {
      const result = enterprises.filter(enterprise => enterprise.enterprise_name.toLowerCase().search(filter.toLowerCase()) !== -1)
      setFindedEnterprises(result)
    }
    if (!filter) {
      setFindedEnterprises([])
    }
  }, [filter])

  useEffect(() => {
    if (!showInputSearch) {
      setFindedEnterprises([])
    }
  }, [showInputSearch])

  async function load (): Promise<void> {
    getEnterprises().then(response => {
      if (response) {
        setEnterprises(response.data.enterprises)
      }
    }).catch(error => {

    })
  }

  return (
    <HomeContext.Provider value={{
      showInputSearch,
      setShowInputSearch,
      enterprises,
      setEnterprises,
      load,
      findedEnterprises,
      setFilter,
      enterpriseCard,
      setEnterpriseCard,
      setShowEntrise,
      showEnterprise
    }}>
      {children}
    </HomeContext.Provider>
  )
}

export default HomeContext
