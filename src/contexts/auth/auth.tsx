import React, { createContext, useState } from 'react'
import { AuthToken } from '../../services/auth_token'
import { login } from '../../services/login'

type AuthContextData = {
  signIn: (email: string, password: string) => void
  isValid: boolean
  loading: boolean
  setIsValid: (isValid: boolean) => void
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData)

export const AuthProvider: React.FC = ({ children }) => {
  const [isValid, setIsValid] = useState<boolean>(true)
  const [loading, setLoading] = useState<boolean>(false)

  async function signIn (email: string, password: string): Promise<void> {
    setLoading(true)
    login(email, password).then(response => {
      if (response) {
        AuthToken.storeToken(response.headers['access-token'], response.headers.client, response.headers.uid)
        setIsValid(true)
        setLoading(false)
      }
    }).catch(error => {
      setIsValid(false)
      setLoading(false)
    })
  }

  return (
    <AuthContext.Provider value={{
      signIn,
      isValid,
      setIsValid,
      loading
    }}>
      {children}
    </AuthContext.Provider>
  )
}

export default AuthContext
