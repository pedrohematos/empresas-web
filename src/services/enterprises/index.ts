import { AxiosResponse } from 'axios'
import { CLIENT_STORAGE_KEY, TOKEN_STORAGE_KEY, UID_STORAGE_KEY } from '../auth_token'
import { axiosInstance } from '../axios-config'
import cookieCutter from 'cookie-cutter'

const endPoint = 'api/v1/enterprises'
export const getEnterprises = async (): Promise<AxiosResponse> => {
  const token = cookieCutter.get(TOKEN_STORAGE_KEY)
  const client = cookieCutter.get(CLIENT_STORAGE_KEY)
  const uid = cookieCutter.get(UID_STORAGE_KEY)

  if (token && client && uid) {
    axiosInstance.defaults.headers['access-token'] = token
    axiosInstance.defaults.headers.client = client
    axiosInstance.defaults.headers.uid = uid
    const result: AxiosResponse = await axiosInstance.get(endPoint)
    return result
  }
}
