import Cookie from 'js-cookie'
import Router from 'next/router'

export const TOKEN_STORAGE_KEY = 'app.token'
export const UID_STORAGE_KEY = 'app.uid'
export const CLIENT_STORAGE_KEY = 'app.client'

export class AuthToken {
  static async storeToken (token: string, client: string, uid: string): Promise<void> {
    Cookie.set(TOKEN_STORAGE_KEY, token)
    Cookie.set(CLIENT_STORAGE_KEY, client)
    Cookie.set(UID_STORAGE_KEY, uid)
    await Router.push('/')
  }
}
