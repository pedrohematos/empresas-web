import { AxiosResponse } from 'axios'
import { axiosInstance } from '../axios-config'

const endPoint = 'api/v1/users/auth/sign_in'
export const login = async (email: string, password: string): Promise<AxiosResponse> => {
  const data = {
    email,
    password
  }
  const result: AxiosResponse = await axiosInstance.post(endPoint, data)
  return result
}
