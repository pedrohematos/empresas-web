import React from 'react'
import Head from 'next/head'
import { Container } from '../styles/pages/components/container'
import Header from '../components/header'
import { Grid } from '@material-ui/core'
import privateRoute from './_withPrivateRoute'
import EnterpriseCard from '../components/enterprise-card'

const Enterprise: React.FC = () => {
  return (
    <Container>
      <Head>
        <title>Enterprise</title>
      </Head>
      <Header />

      <Grid container justify="center" alignItems="center" style={{ height: '100vh' }}>
        <Grid container justify="center" alignItems="center" >
          <Grid item xs={11} sm={5} md={5}>
            <EnterpriseCard/>
          </Grid>
        </Grid>

      </Grid>

    </Container>
  )
}

export default privateRoute(Enterprise)
