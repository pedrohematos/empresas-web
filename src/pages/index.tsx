import React, { useContext, useEffect } from 'react'
import Head from 'next/head'
import { Container } from '../styles/pages/components/container'
import Header from '../components/header'
import { Grid } from '@material-ui/core'
import HomeContext from '../contexts/home/home'
import CardFound from '../components/card'
import privateRoute from './_withPrivateRoute'

const Home: React.FC = () => {
  const { showInputSearch, findedEnterprises, load } = useContext(HomeContext)

  useEffect(() => {
    load()
  },[])

  return (
    <Container>
      <Head>
        <title>Home</title>
      </Head>
      <Header />

      <Grid container justify="center" alignItems="center">
        {
          findedEnterprises.length === 0 && !showInputSearch ? (
            <Grid container spacing={2} justify="center" alignItems="center" style={{ height: '100vh' }}>
              <Grid item>
                <h3 className="home-title">Clique na busca para iniciar.</h3>
              </Grid>
            </Grid>

          )
            : (
              <Grid container spacing={2} justify="center" alignItems="center" style={{ marginTop: 80 }}>
              {
                findedEnterprises.length !== 0 &&
                  findedEnterprises.map((enterprise) => (
                    <Grid item xs={12} sm={11} md={11} key={enterprise.id}>
                      <CardFound
                        description={enterprise.description}
                        name={enterprise.enterprise_name}
                        type={enterprise.enterprise_type.enterprise_type_name}
                        country={enterprise.country}
                        image={enterprise.photo}
                        key={enterprise.id}
                      />
                    </Grid>
                  ))
                }
              </Grid>
            )
        }

      </Grid>

    </Container>
  )
}

export default privateRoute(Home)
