import React, { Component } from 'react'
import ServerCookie from 'next-cookies'
import { NextPageContext } from 'next'
import { TOKEN_STORAGE_KEY } from '../services/auth_token'
import { redirectToLogin } from '../services/redirect_service'

export type AuthProps = {
  token: string
}

const privateRoute = (WrappedComponent: any): any => {
  return class PrivateRoute extends Component {
    state = {
      auth: ''
    }

    static async getInitialProps (ctx: NextPageContext): Promise<object> {
      const token = ServerCookie(ctx)[TOKEN_STORAGE_KEY]
      const initialProps = { token }
      if (!token) redirectToLogin(ctx.res)
      if (WrappedComponent.getInitialProps) {
        const wrappedProps = await WrappedComponent.getInitialProps(initialProps)
        return { ...wrappedProps, token }
      }
      return initialProps
    }

    componentDidMount (): void {
      this.setState({ auth: '' })
    }

    render (): JSX.Element {
      const { ...propsWithoutAuth } = this.props
      return <WrappedComponent auth={this.state.auth} {...propsWithoutAuth} />
    }
  }
}

export default privateRoute
