import React, { useState, useContext } from 'react'
import { Grid, IconButton, InputAdornment, TextField } from '@material-ui/core'
import Button from '../components/button'
import Head from 'next/head'
import logo from '../assets/logo-home@2x.png'

import AuthContext from '../contexts/auth/auth'
import {
  Visibility,
  VisibilityOff,
  ErrorRounded,
  LockOpenRounded,
  MailOutline
} from '@material-ui/icons'
import Loading from '../components/loading'

const Login: React.FC = () => {
  const { signIn, isValid, loading } = useContext(AuthContext)
  const [showPassword, setShowPassword] = useState<boolean>(true)
  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')

  const handleLogin = (): void => {
    signIn(email, password)
  }  
  
  return (
    <Grid container justify="center" alignItems="center" style={{ height: '100vh' }}>
      <Head>
        <title>Login</title>
      </Head>
      <Grid item xs={12} md={3} sm={3} >
        <Grid container spacing={4} className="login">
          
          {/**Logo*/}
          <Grid item xs={11} md={12} sm={12}>
            <Grid container justify="center" alignItems="center">
              <img src={logo} alt="Logo Ioasys"/>
            </Grid>
          </Grid>
          
          {/**Title*/}
          <Grid item xs={11} md={12} sm={12}>
            <Grid container justify="center" alignItems="center">
              <h3 className="login-title">BEM-VINDO AO <br />
                  EMPRESAS</h3>
            </Grid>
          </Grid>
          
          {/**Subtitle*/}
          <Grid item xs={11} md={12} sm={12}>
            <Grid container justify="center" alignItems="center">
              <p className="login-sub--title">Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
            </Grid>
          </Grid>
          
          {/**Email text input*/}
          <Grid item xs={11} md={12} sm={12}>
            <Grid container justify="center" alignItems="center">
              <TextField
                placeholder="E-mail"
                onChange={(e) => setEmail(e.target.value)}
                fullWidth
                variant="standard"
                type="email"
                InputProps={{
                  startAdornment:
                    <InputAdornment position="start">
                      <MailOutline color="primary" />
                    </InputAdornment>,
                  endAdornment: !isValid && (
                    <InputAdornment position="start">
                      <ErrorRounded className="login-error--color" />
                    </InputAdornment>
                  )
                }}
              />
            </Grid>
          </Grid>

          {/**Password text input*/}
          <Grid item xs={11} md={12} sm={12}>
            <Grid container justify="center" alignItems="center">
              <TextField
                placeholder="Senha"
                onChange={(e) => setPassword(e.target.value)}
                value={password}
                fullWidth
                type={showPassword ? 'text' : 'password'}
                variant="standard"
                InputProps={{
                  startAdornment:
                    <InputAdornment position="start">
                      <LockOpenRounded color="primary" className="login-lock--icon" />
                    </InputAdornment>,
                  endAdornment: isValid && password
                    ? (<InputAdornment position="start">
                      <IconButton
                        onClick={() => setShowPassword(!showPassword)}
                        onMouseDown={(e) => (e.preventDefault())}
                        edge="end"
                      >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>

                    )
                    : !isValid && (
                      <InputAdornment position="start">
                        <ErrorRounded className="login-error--color" />
                      </InputAdornment>
                    )
                }}

              />
            </Grid>
          </Grid>
          
          {/**Invalid login message*/}
          {
            !isValid && (
              <Grid item xs={11} md={12} sm={12}>
                <Grid container justify="center" alignItems="center">
                  <span className="login-error">Credenciais informadas são inválidas, tente novamente.</span>
                </Grid>
              </Grid>

            )
          }

          {/* "Entrar" Button */}
          <Grid item xs={11} md={12} sm={12}>
            <Grid container justify="center" alignItems="center">
              <Button onClick={handleLogin} invalid={!isValid} />
            </Grid>
          </Grid>

        </Grid>
      
      {/**Loading Status */}
      </Grid>
      {loading && <Loading />}
    </Grid>
  )
}

export default Login
