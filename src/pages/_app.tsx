import React from 'react'
import { AppProps } from 'next/app'
import '../styles/sass/styles.scss'
import { ThemeProvider } from '@material-ui/core'
import { theme } from '../styles/theme'
import { AuthProvider } from '../contexts/auth/auth'
import { HomeProvider } from '../contexts/home/home'

const App: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <AuthProvider>
      <HomeProvider>
        <ThemeProvider theme={theme}>
          <Component {...pageProps} />
        </ThemeProvider>
      </HomeProvider>
    </AuthProvider>
  )
}

export default App
