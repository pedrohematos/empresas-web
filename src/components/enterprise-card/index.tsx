
import React, { useContext } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import HomeContext from '../../contexts/home/home'

const useStyles = makeStyles({
  root: {
    maxWidth: 600
  },
  header: {
    height: 300
  },
  content: {
    maxHeight: 300,
    height: 200

  }
})

const EnterpriseCard: React.FC = () => {
  const { enterpriseCard } = useContext(HomeContext)
  const classes = useStyles()

  return (
    <Card className={classes.root}>
      <CardActionArea>

        <CardMedia
          className={classes.header}
          component="img"
          alt="EnterpriseCard"
          height="140"
          image={enterpriseCard.image}
        />

        <CardContent className={classes.content}>
          <Typography variant="body2" color="textSecondary" component="p">
            {enterpriseCard.description}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export default EnterpriseCard
