import React from 'react'

type ButtonProps = {
  onClick: any
  invalid: boolean
}

const Button: React.FC<ButtonProps> = ({ invalid, onClick }) => {
  return <button
    type="button"
    className="button"
    onClick={onClick}
    style={{ background: invalid ? '#748383' : '#57bbbc' }}
  >
    Entrar
  </button>
}

export default Button
