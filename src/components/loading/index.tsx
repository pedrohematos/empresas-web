import React from 'react'
import { CircularProgress } from '@material-ui/core'

const Loading: React.FC = () => {
  return (
    <div className="loading">
      <CircularProgress size={100} disableShrink />
    </div>
  )
}

export default Loading
