import React, { useContext } from 'react'
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import { useRouter } from 'next/router'
import HomeContext from '../../contexts/home/home'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      height: 150,
      padding: 15,
      cursor: 'pointer'
    },
    details: {
      display: 'flex',
      flexDirection: 'column'
    },
    content: {
      flex: '1 0 auto'
    },
    cover: {
      width: 251
    }
  })
)

type CardProps = {
  name: string
  type: string
  country: string
  image: string
  description: string
}

const CardFound: React.FC<CardProps> = ({ name, type, country, image, description }) => {
  const classes = useStyles()
  const router = useRouter()
  const { setEnterpriseCard, setShowEntrise } = useContext(HomeContext)

  const img = `https://empresas.ioasys.com.br/${image}`

  const openEnterpriseCard = (): void => {
    if (description && image) {
      setShowEntrise(true)
      setEnterpriseCard({
        description: description,
        image: img,
        name: name
      })

      router.push('/enterprise')
    }
  }

  return (
    <Card className={classes.root} onClick={openEnterpriseCard}>
      <CardMedia
        className={classes.cover}
        image={img}
        title="Live from space album cover"
      />
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <Typography component="h5" variant="h5">
            {name}
          </Typography>
          <Typography variant="h6">
            {type}
          </Typography>
          <Typography variant="subtitle1" color="textSecondary">
            {country}
          </Typography>
        </CardContent>
      </div>
    </Card>
  )
}

export default CardFound
