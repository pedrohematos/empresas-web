import React, { useContext } from 'react'
import logo from '../../assets/logo-nav.png'
import SearchIcon from '@material-ui/icons/Search'
import CloseIcon from '@material-ui/icons/Close'
import { TextField, InputAdornment, IconButton } from '@material-ui/core'
import HomeContext from '../../contexts/home/home'
import { useRouter } from 'next/router'
import { ArrowBack } from '@material-ui/icons'

const Header: React.FC = () => {
  const router = useRouter()
  const { showInputSearch, setShowInputSearch, setFilter, showEnterprise, enterpriseCard, setShowEntrise } = useContext(HomeContext)

  const backToHome = (): void => {
    setShowEntrise(false)
    router.push('/')
  }

  return (
    <header className="header__container">
      <nav className="header__container-nav">
        {showEnterprise
          ? (
            <>
              <h3 className="enterpriseName">{enterpriseCard.name}</h3>
              <button className="button-back" onClick={backToHome}>
                <ArrowBack/>
              </button>
            </>

          )
          : (

            <>
              {
                !showInputSearch
                  ? (<>
                    <img src={logo} alt="Logo Nav"/>
                    <button className="button-search" onClick={() => setShowInputSearch(!showInputSearch)}>
                      <SearchIcon/>
                    </button>
                  </>
                  )
                  : (<TextField
                    placeholder="Pesquisar"
                    onChange={(e) => setFilter(e.target.value)}
                    fullWidth
                    color="secondary"
                    className="inputSearch"
                    variant="standard"
                    InputProps={{
                      startAdornment:
                    <InputAdornment position="start">
                      <SearchIcon className="searchIcon" />
                    </InputAdornment>,
                      endAdornment: <InputAdornment position="start">
                        <IconButton
                          onClick={() => setShowInputSearch(!showInputSearch)}
                          onMouseDown={(e) => (e.preventDefault())}
                          edge="end"
                        >
                          <CloseIcon className="searchIcon" />
                        </IconButton>
                      </InputAdornment>

                    }}
                  />)
              }
            </>

          )

        }
      </nav>
    </header>)
}

export default Header
