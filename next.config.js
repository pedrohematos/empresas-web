const withSass = require('@zeit/next-sass')
const withCss = require('@zeit/next-css')
const withImage = require('next-images')
module.exports = withCss(withSass(withImage({ esModule: true })))
